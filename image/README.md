# Reglas para las Imágenes

## Portadas de Capítulos

Todos los capítulos deben tener una imagen de portada. 

### Convención de Nombrado
Las mismas deben nombrarse según el número de su capítulo siguiendo la convención: *Chapter<Chapter_Number>.<file_extension>*.

Así, la imagen portada del capítulo 1 se llamará *Chapter1.jpg*, mientras que la del capitulo 23 se llamará *Chapter23.png*.

### Especificaciones de la Imagen
La definición de la imagen y si tamaño son importantes dado que se debe mantener una calidad mínima de imagen, y los mismos márgenes
en cada portada. Las imágenes no son procesadas por el compilador latex, por lo que una imagen demasiado larga, o corta, hará que los
márgenes de la portada se alteren.

Para mantener la calidad de las portadas, las imágenes usadas deberán tener **1024x520 px**.
![](../README/image_size.jpg)