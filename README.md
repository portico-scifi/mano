# M.A.N.O.
El Manual Asombrosamente Notariado para Overlords de Pórtico.

![Gitlab pipeline status](https://img.shields.io/gitlab/pipeline/portico-scifi/mano?style=for-the-badge)
![Edición Pórtico](https://img.shields.io/badge/P%C3%B3rtico%20Edici%C3%B3n-2020-cornflowerblue?style=for-the-badge&logo=wordpress&link=http://portico.site&link=http://portico.site)

Descargá la última versión: [aquí](https://gitlab.com/portico-scifi/mano/-/jobs/artifacts/master/download?job=build).

## Que es M.A.N.O.?
Es un documento donde se detalla que es Pórtico: Encuentro de Ciencia Ficción,
y como llevar adelante una edición del mismo evento.

Se compone de tres partes igualmente importantes, en las cuales se explica cual
es la esencia de Pórtico, como darle impronta propia a cada edición de Pórtico,
y algunas ideas, herramientas, dinámicas y/o procesos que se han utilizado en 
Pórtico en alguna de sus ediciones anteriores.

## Dependencias

El template usado usa el módulo *pgfornament* incluido por default en las últimas versiones de TexLive LaTeX.

Como el compilador online del que hace uso este repositorio parece no estar actualizado con dichas versiones, 
hemos incluido en la raíz del proyecto los archivos básico necesarios para compilar el PDF.

Dichos archivos fueron descargados de: https://ctan.org/tex-archive/macros/latex/contrib/tkz/pgfornament.

## Licencia

Este documento hace uso del template LaTeX [ElegantBook](https://elegantlatex.org/).

![License](https://img.shields.io/ctan/l/elegantbook.svg)
